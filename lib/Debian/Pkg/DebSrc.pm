#!/usr/bin/perl -w

##  Copyright (C) 2001  Denis Barbier <barbier@debian.org>
##  Copyright (C) 2017  Pino Toscano <pino@debian.org>
##
##  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation; either version 2 of the License, or
##  (at your option) any later version.

=head1 NAME

Debian::Pkg::DebSrc - extract contents from Debian source package

=head1 SYNOPSIS

 use Debian::Pkg::DebSrc;
 my $deb = Debian::Pkg::DebSrc->new("/path/to/foo_0.1-1.dsc");
 my $body = $deb->file_content("debian/control");

=head1 DESCRIPTION

This module extracts informations and files from a Debian source
package.

=head1 METHODS

=over 4

=cut

package Debian::Pkg::DebSrc;

use Dpkg::Source::Package;
use Dpkg::Control::Info;
use File::Copy;
use File::Find;
use File::Slurp;
use File::Spec;
use File::Temp;

use strict;
use Carp;

=item new

This is the constructor.

   my $deb = Debian::Pkg::DebSrc->new("/path/to/foo_0.1-1.dsc");

Basically, C<dsc> file is parsed, and extracted (using
C<Dpkg::Source::Package>) in a temporary directory.

Optional arguments are:

   my $deb = Debian::Pkg::DebSrc->new("/path/to/foo_0.1-1.dsc",
        tmpdir => "/my/temp/path",
        debug => 1,
   );

=cut

sub new {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $file = shift;

        my $tmpdir = '';
        my $debug = 0;
        if ($#_ >= 0) {
                my %opts = @_;
                while (my ($key, $val) = each %opts) {
                        if ($key eq 'tmpdir') {
                                $tmpdir = File::Spec->rel2abs($val);
                        } elsif ($key eq 'debug') {
                                $debug = $val;
                        }
                }
        }

        $tmpdir = File::Spec->tmpdir() if $tmpdir eq '';

        my $pkgtmpdir = File::Temp->newdir("debsrcXXXXX", DIR => $tmpdir) or return undef;

        my $pkg = Dpkg::Source::Package->new(filename => $file) or return undef;
        $pkg->extract($pkgtmpdir) or return undef;

        my @files = ();
        my $iter = sub {
                return unless -f $_;
                my $relpath = File::Spec->abs2rel($File::Find::name, $pkgtmpdir);
                push (@files, $relpath);
        };
        my $preproc = sub {
                #  Do not consider version control files
                return grep { $_ ne '.pc' && $_ ne '{arch}' && $_ ne 'CVS' && $_ ne '.arch-ids' && $_ ne '.svn' } @_;
        };
        File::Find::find({wanted => $iter, preprocess => $preproc}, $pkgtmpdir);

        my $self = {
                pkg => $pkg,
                pkgtmpdir => $pkgtmpdir,
                files => [@files],
                debug => $debug,
        };
        bless ($self, $class);

        return $self;
}

sub _debug {
        my $self = shift;
        return unless $self->{debug} >= 2;
        print STDERR __PACKAGE__." Debug: ".$_[0]."\n";
}

=item copy_files

Copies files from the extracted source package.

The specified argument is the function returning a full path, in case
a file must be copied, or undef.

   sub pick_rules {
        my $file = shift;
        return "/local/dir/rules" if $file eq "debian/rules";
        return undef;
   }
   $deb->copy_files(\&pick_rules);

=cut

sub copy_files {
        my $self = shift;
        my $matchfiles = shift;

        if (ref($matchfiles) ne 'CODE') {
                Carp::confess "Invalid argument of ".__PACKAGE__."::copy_files";
        }

        foreach my $file (@{$self->{files}}) {
                my $path = &$matchfiles($file);
                if ($path) {
                        my $dir = File::Basename::dirname($path);
                        File::Path::mkpath($dir, 0, 0755);
                        File::Copy::copy(File::Spec->rel2abs($file, $self->{pkgtmpdir}), $path);
                }
        }
}

=item get_diff_name

Returns the full qualified name of the diff file, or empty string if it
does not exist.

   my $patchname = $deb->get_diff_name();

=cut

sub get_diff_name {
        my $self = shift;

        foreach my $file ($self->{pkg}->get_files()) {
                return $file if $file =~ /\.diff\.gz$/
        }

        return '';
}

=item file_matches

Check files matching in the source package

=cut

sub file_matches {
        my $self = shift;
        my $expr = shift;
        my @found = ();

        my $match = sub { my $file = shift; $file =~ m/$expr/; };
        foreach my $file (@{$self->{files}}) {
                push (@found, $file) if &$match($file);
        }

        return @found;
}

=item file_exists

Check if a given file exists in the source package

=cut

sub file_exists {
        my $self = shift;
        my $file = shift;

        return -e File::Spec->rel2abs($file, $self->{pkgtmpdir});
}

=item file_content

Get the content of a file from the source package

=cut

sub file_content {
        my $self = shift;
        my $file = shift;

        my $fullpath = File::Spec->rel2abs($file, $self->{pkgtmpdir});

        $self->_debug("Retrieve content of file $file");
        unless (-e $fullpath) {
                Carp::carp "File \`$file' not found in archive";
                return;
        }

        return File::Slurp::read_file($fullpath);
}

=item get_control

Get the 'control' file of the source package, as C<Dpkg::Control::Info>
object.

=cut

sub get_control {
        my $self = shift;

        my $fullpath = File::Spec->catfile($self->{pkgtmpdir}, "debian/control");
        my $control = Dpkg::Control::Info->new($fullpath);
        return $control;
}

=back

=head1 LIMITATIONS

The source is fully extracted, so it will require the available disk
space for it.  Also, patches applied during the build (for example in
v1 sources, and in v3 sources that apply them manually) are not
considered, when extracting files.

=head1 AUTHOR

Copyright (C) 2001  Denis Barbier <barbier@debian.org>
Copyright (C) 2017  Pino Toscano <pino@debian.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

=cut

1;

