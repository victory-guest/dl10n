#!/bin/sh
set -e

# update_unstable.sh
#
# Update the RRD database for the unstable suite, and generate the graphs.
# RRD and graphs are also generated for the unstable manpages.
#
# Copyright (C) 2007 Nicolas François
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

GRAPH_SCRIPTS_DIR=$(dirname $0)
. $GRAPH_SCRIPTS_DIR/config.sh

dist=unstable

export LC_ALL=C

cd $RRD_HOME/${dist}/
if [ ! -e /srv/i18n.debian.net/www/debian-l10n-material/data/$dist.gz ]
then
    echo "No /srv/i18n.debian.net/www/debian-l10n-material/data/$dist.gz! Exiting."
    exit 1
fi
gunzip -c /srv/i18n.debian.net/www/debian-l10n-material/data/$dist.gz > $dist

PERLLIB=$DL10N_HOME/lib $DL10N_HOME/dl10n-rrd/dl10n-rrd --db=$dist

for period in week month year
do
    if [ ! -d "$period" ]
    then
        mkdir "$period"
    fi
    for fmt in po po4a podebconf
    do
# Hardcoded list of languages, based on the current ranks (2007 03 03)
        range=""
        case $fmt in
            po)
                languages="fr de es it sv ru nl ja pt_BR pl cs fa" ;;
            podebconf)
                range="--upper-limit 11000 --lower-limit 5000 --rigid";
                languages="fr cs de sv vi ja nl es pt_BR pt ru gl fa" ;;
            po4a)
                languages="fr es ja ru de ca pl it sv pt_BR uk ko el tr hu fa" ;;
        esac
        $GRAPH_SCRIPTS_DIR/graph_ranks.sh \
            -o "$period/$fmt.png" \
            "$fmt" \
            $languages \
            -- \
            --zoom 1.5 --start end-1$period --end 00:00+1d \
            $range

        case $fmt in
            podebconf)
                range="--upper-limit 11000 --lower-limit 0 --rigid";;
        esac
        for lang in $fmt/*.rrd
        do
            lang=$(basename $lang)
            lang=${lang%.rrd}
            $GRAPH_SCRIPTS_DIR/graph_lang.sh \
                -o "$period/$fmt-$lang.png" \
                "$fmt" \
                "$lang" \
                $range \
                --start end-1$period --end 00:00+1d
        done
    done
done

rm -f $dist

#
# man pages
#
$DL10N_HOME/dl10n-rrd/manpages-rrd.pl $dist

languages="ja fr es pl de ko zh_CN zh_TW pt it ru fa"
$GRAPH_SCRIPTS_DIR/manpages_graph_ranks.sh \
    $languages \
    -- \
    --zoom 1.5 \
    --start end-1month --end 00:00+1d

