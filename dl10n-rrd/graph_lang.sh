#!/bin/bash
set -e

# graph_lang.sh
#
# Create a graph representing the variations of the translated, fuzzy and
# untranslated strings for the given format and language.
# The statistics are displayed as an histogram.
#
# Copyright (C) 2007 Nicolas François
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

rrdtool_options="--start now-2d"

if [ $# -lt 2 ]
then
        echo "Usage: $0 [-o file] <format> <language> [rrdtool_options]" >&2
        echo "formats: po, podebconf, po4a"  >&2
        echo "rrdtool_options defaults to $rrdtool_options" >&2
        exit 1
fi


if [ "$1" = "-o" ]
then
    output="$2"
    shift 2
fi


fmt=$1
lang=$2

if [ -z "$output" ]
then
    output="$fmt-$lang.png"
fi

if [ $# -gt 2 ]
then
    rrdtool_options=${@:3}
fi

if [ ! -f $fmt/$lang.rrd ]
then
        echo "No such rrd file: $fmt/$lang.rrd" >&2
        exit 1
fi

export LC_ALL=C

#echo -n "Generating $fmt-$lang.png... "
rrdtool graph "$output" \
         --lower-limit 0 \
         --units-exponent 0 \
         --height 200 \
         --title "$fmt strings ($lang)" \
         --vertical-label "strings" \
         $rrdtool_options \
         DEF:t=$fmt/$lang.rrd:t:AVERAGE \
         DEF:f=$fmt/$lang.rrd:f:AVERAGE \
         DEF:u=$fmt/$lang.rrd:u:AVERAGE \
         DEF:total=$fmt/__.rrd:u:AVERAGE \
         CDEF:nopo=total,t,-,f,-,u,- \
         AREA:t#00FF00:translated:STACK \
         AREA:f#0000FF:fuzzy:STACK \
         AREA:u#FF0000:untranslated:STACK \
         AREA:nopo#FFFF00:"no po":STACK \
         COMMENT:"$(date|sed 's/:/\\:/g')" \
         COMMENT:"Comments\: debian-l10n-devel@lists.alioth.debian.org" \
         > /dev/null
#echo "done."

