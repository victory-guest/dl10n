#!/bin/bash
set -e

# graph_ranks.sh
#
# Create a graph representing the variations of translated manpages for
# the given languages
#
# Copyright (C) 2007 Nicolas François
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#

if [ $# -lt 2 ]
then
        echo "Usage: $0 <languages> [-- rrdtool_options]" >&2
        echo "formats: po, podebconf, po4a"  >&2
        echo "rrdtool_options defaults to $rrdtool_options" >&2
        exit 1
fi

. $(dirname $0)/colors.sh

rrdtool_options="--start now-2d"
firstlang=$1
shift
languages=$firstlang
n=1

while true
do
    arg=$1
    if [ -z "$arg" ]
    then
        # End of languages & rrdtool options
        break
    else
        if [ "$arg" = "--" ]
        then
            # Begining of the rrdtool options
            shift
            rrdtool_options=$@
            break
        else
            languages="$languages $arg"
            n=$((n+1))
        fi
    fi

    # Next argument
    shift
done

export LC_ALL=C

a=0

#echo -n "Generating man.png... "
rrdtool graph man.png \
                 --units-exponent 0 \
                 --lower-limit 0 \
                 --height 200 \
                 --title "manpages" \
                 --vertical-label "manpages (%)" \
                 $rrdtool_options \
                 DEF:total1=man/_.rrd:translated:AVERAGE \
                 DEF:total2=man/_.rrd:only:AVERAGE \
                 CDEF:total=total1,total2,+ \
                 $(for l in $languages; do
                     echo -n " DEF:${l}1=man/$l.rrd:translated:AVERAGE"
                     echo -n " DEF:${l}2=man/$l.rrd:only:AVERAGE"
                     echo -n " CDEF:$l=${l}1,${l}2,+,total,/,100,*"
                   done) \
                 $(for l in $languages; do
                     echo -n " LINE:$l#$(get_color $n $a):$l"
                     a=$((a+1))
                   done) \
                 COMMENT:"$(date|sed 's/:/\\:/g')" > /dev/null
#echo "done."

